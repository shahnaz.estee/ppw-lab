from django.db import models

# Create your models here.
from django.db import models

class Matkul(models.Model):
    mata_kuliah  = models.CharField(max_length=30)
    dosen_pengajar  = models.CharField(max_length=50)
    jumlah_SKS = models.IntegerField()
    deskripsi = models.CharField(max_length=100)
    semester_tahun = models.IntegerField()
    ruang_kelas = models.CharField(max_length=6)
