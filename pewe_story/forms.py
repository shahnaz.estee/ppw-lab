from django import forms
from .models import Matkul

class InputForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = ['mata_kuliah','dosen_pengajar','jumlah_SKS','deskripsi','semester_tahun','ruang_kelas']