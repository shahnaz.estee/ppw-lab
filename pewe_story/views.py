from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from .models import Matkul
from .forms import InputForm
from django.forms import ModelForm

# Create your views here.
def contact(request):
    return render(request,'contact.html')

def index(request):
    return render(request,'index.html')

from django.shortcuts import render
from django.http import HttpResponse, Http404

def courselist(request):
    matakuliah= Matkul.objects.all()
    return render(request, 'story5.html', {'data': matakuliah})

def addform(request):
    if request.method == "POST":
        form= InputForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('course list')
    else:
        form= InputForm()
    return render(request, 'form5.html', {'form': form})

#def deleterow(request,pk):
    #obj = {'data':matakuliah}[pk]
    #obj.delete()
    #return redirect('course list')
